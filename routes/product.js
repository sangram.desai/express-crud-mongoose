var express = require('express');
var router = express.Router();
var Product = require('../models/product');




router.get('/', function (req, res, next) {
    Product.find({}, function (err, product) {
        if (err) {
            res.json({ status: false, data: {}, message: err.toString() })
        } else {
            res.json({
                status: true,
                data: product,
                message: "All Product Information"
            })
        }
    })
});

router.get('/:code', function (req, res, next) {
    console.log(req.params.code);


    Product.find({ code: req.params.code }, function (err, product) {
        if (err) {
            res.json({ status: false, data: {}, message: err.toString() })
        } else {
            res.json({
                status: true,
                data: product,
                message: "Individual Product Information"
            })
        }
    })
});

router.put('/:code', function (req, res, next) {
    Product.findOneAndUpdate({ code: req.params.code },
        {
            $set: {
                name: req.body.name,
                price: req.body.price
            }
        },
        { new: true }
        , function (err, product) {

            if (err) {
                res.json({ status: false, data: {}, message: err.toString() })
            } else {
                res.json({
                    status: true,
                    data: product,
                    message: "Individual Product Updated"
                })
            }
        });
});


router.delete('/:code', function (req, res, next) {
    Product.findOneAndRemove({ code: req.params.code}, function(err,product) {
        if (err){
            res.json({ status: false, data: {}, message: err.toString() })
        } else{
            res.json({
                status: true,
                data: product,
                message: "Individual Product deleted"
            })
        }
      });
});


router.post('/', function (req, res, next) {
    var newProduct = Product({
        name: req.body.name,
        code: req.body.code,
        price: req.body.price,
        inserted: Date.now(),
        updated: Date.now()
    });

    newProduct.save(function (err) {
        if (err) {
            res.json({ status: false, data: {}, message: err.toString() })
        } else {
            res.json({
                status: true,
                data: newProduct,
                message: "New Product Created"
            })
        }


    });
});

module.exports = router;
