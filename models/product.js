var mongoose = require('mongoose');
var Schema = mongoose.Schema;

mongoose.connect('mongodb://localhost:27017/test', { useNewUrlParser: true });


var productSchema = new Schema({
    name: { type: String, minlength: 5, maxlength: 15 },
    code: { type: String, minlength: 5, maxlength: 15 },
    price: { type: Number },
    inserted: { type: Date, default: Date.now },
    updated: { type: Date },
});


var Product = mongoose.model('Product', productSchema);
module.exports = Product